<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        return view('projects.index');
    }

    public function getProject(Project $project) 
    {
        return json_encode($project);
    }

    public function getProjects()
    {
        $projects = Project::with('client')->get();

        return json_encode($projects);
    }

    public function show(Project $project) 
    {
        return view('projects.view', compact('project'));
    }

    public function store(Request $request)
    {
        $project = new Project;
        $project->fill($request->input());
        $project->save();

        return json_encode((object) [
            'status' => 1,
            'message' => 'Nieuw project aangemaakt.'
        ]);
    }

    public function update(Request $request, Project $project)
    {
        $project->fill($request->input())->save();

        return json_encode((object) [
            'message' => 'Project aangepast.',
            'status' => 1
        ]);
    }

    public function destroy(Project $project)
    {
        $project->delete();

        return json_encode((object) [
            'message' => 'Project verwijderd.',
            'status' => 1
        ]);
    }
}
