<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timer extends Model
{
    protected $fillable = [
    	'type', 'date', 'time', 'description', 'user_id', 'client_id'
    ];
}
