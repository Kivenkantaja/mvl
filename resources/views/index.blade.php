@extends('layout')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="pull-left">
				<h1>Home</h1>
			</div>
			<div class="pull-right">
				<button onclick="window.location.reload(true);" class="btn btn-table">
					<i class="fas fa-history"></i>
				</button>
			</div>
		</div>
	</div>

    <reminders v-on:message="message = $event"></reminders>
@stop
