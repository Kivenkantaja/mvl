@extends('layout')

@section('content')
	<h1>Uren</h1>

	<hours :user="{{ Auth::user() }}" v-on:message="message = $event" v-on:form="hourForm = $event" ref="hours"></hours>
@stop
