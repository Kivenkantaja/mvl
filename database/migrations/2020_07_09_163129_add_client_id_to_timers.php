<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClientIdToTimers extends Migration
{
    public function up()
    {
        Schema::table('timers', function (Blueprint $table) {
            $table->integer('client_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('timers', function (Blueprint $table) {
            $table->dropColumn('client_id');
        });
    }
}
