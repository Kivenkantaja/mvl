<?php

namespace App\Http\Controllers;

use App\Timer;
use Illuminate\Http\Request;
use Auth;

use Carbon\Carbon;

class TimerController extends Controller
{
    public function index(Request $request) 
    {
        Timer::where('date', '!=', date('Y-m-d'))->delete();

        return json_encode(Timer::where('user_id', Auth::user()->id)->where('type', $request->type)->get());
    }

    public function store(Request $request)
    {
        Timer::create($request->input());

        return json_encode(Timer::where('type', $request->type)->get());
    }

    public function show(Timer $timer)
    {
        return json_encode($timer);
    }
    
    public function update(Request $request, Timer $timer)
    {
        $timer->update($request->input());
    }
}
