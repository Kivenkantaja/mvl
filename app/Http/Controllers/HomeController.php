<?php

namespace App\Http\Controllers;

use App\Hour;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('index');
    }

    public function dayReports(Request $request)
    {
        $range = $request->input();

        foreach($range as $key => $date) {
            $hours = Hour::where('date', $date)->with('project')->get();

            $range[$key] = [
                'date' => $date,
                'hours' => count($hours) > 0 ? $hours : null
            ];
        }

        return json_encode($range);
    }
}
