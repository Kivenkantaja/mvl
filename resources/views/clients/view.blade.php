@extends('layout')

@section('content')
	<client v-on:message="message = $event;" 
			v-on:form="hourForm = $event;"
			client_id="{{ $client->id }}"
			ref="client">			
	</client>
@stop