<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    public function getUsers()
    {
        return json_encode(User::get());
    }

    public function store(Request $request)
    {
        User::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Gebruiker aangemaakt.'
        ]);
    }

    public function show(User $user)
    {
        return view('users.view', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $user->update($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Gebruiker bijgewerkt.'
        ]);        
    }

    public function destroy(User $user)
    {
        $user->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Gebruiker verwijderd.'
        ]);            
    }
}
