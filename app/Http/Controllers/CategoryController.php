<?php

namespace App\Http\Controllers;

use App\Category;
use App\Material;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('categories.index');
    }

    public function create()
    {
        //
    }

    public function getCategories() 
    {
        return json_encode(Category::get());
    }

    public function store(Request $request)
    {
        Category::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Categorie opgeslagen.',
        ]);
    }

    public function update(Request $request, Category $category)
    {
        $category->update($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Categorie bijgewerkt.'
        ]);
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return json_encode([
            'status' => 1,
            'message'=> 'Categorie verwijderd.'
        ]);
    }
}
