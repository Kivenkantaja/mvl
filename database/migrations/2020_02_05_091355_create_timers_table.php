<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimersTable extends Migration
{

    public function up()
    {
        Schema::create('timers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('date');
            $table->string('time');
            $table->integer('type'); // 0 = normal, 1 = mw
            $table->string('description')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('timers');
    }
}
