<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    protected $fillable = [
    	'startTime',
		'endTime',
        'break_minutes',
		'description',
		'date',
		'project_id',
		'mw',
		'user_id',
		'break',
        'client_id',
        'recording'
	];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function project()
    {
    	return $this->belongsTo('App\Project');
    }
}
