<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
	use SoftDeletes;

    protected $fillable = ['name', 'status', 'address', 'postal_code', 'town', 'phone_number', 'last_used'];

    public function projects() 
    {
    	return $this->hasMany('App\Project');
    }

    public function hours()
    {
    	return $this->hasMany('App\Hour');
    }
}
