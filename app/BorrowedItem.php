<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowedItem extends Model
{
    protected $fillable = [
        'name', 'person', 'date',
    ];
}
