<?php

namespace App\Http\Controllers;

use App\Hour;
use App\Project;
use App\Timer;
use App\Client;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;
use Auth;

class HourController extends Controller
{
    public function index()
    {
        return view('hours.index');
    }

    public function store(Request $request)
    {
        $hours = new Hour;
        $hours->fill($request->input())->save();

        if($request->timer) {
            Timer::destroy($request->timer);
        }

        if($request->client_id) {
            Client::find($request->client_id)->update(['last_used' => Carbon::now()]);
        }

        return json_encode((object) [
            'status' => 1,
            'message' => 'De uren zijn opgeslagen.'
        ]);
    }

    public function update(Request $request, Hour $hour)
    {
        $hour->update($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Uren bijgewerkt.'
        ]);
    }

    public function destroy(Hour $hour)
    {
        $hour->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Uren verwijderd.'
        ]);
    }

    public function getHours(Request $request)
    {
        $hours = Hour::with(['user', 'client', 'project' => function($project) {
            $project->with('client');
        }]);

        if ($request->client_id) {
            $hours->where('client_id', $request->client_id);
        }

        if ($request->users) {
            $hours->whereIn('user_id', $request->users);
        }

        if ($request->clients) {
            $hours->whereIn('client_id', $request->clients);
        }

        if (!Auth::user()->is_admin) {
            $hours->where('user_id', Auth::user()->id);
        }

        $hours = $hours->get();

        return response()->json($hours);
    }

    public function getHoursOverview(Request $request)
    {
        $options = json_decode($request->options);

        $projects = Project::whereHas('hours', function($q) use ($request) {
            $q->whereIn('date', $request->range);
        })->with(['hours' => function($q) use ($request) {
            $q->whereIn('date', $request->range);
            $q->with('user');
        }, 'client']);

        if($options && $options->client_id) {
            $projects = $projects->where('client_id', $options->client_id);
        }

        return json_encode($projects->get());
    }

    public function hoursByProject($project_id)
    {
        return json_encode(Hour::where('project_id', $project_id)->with('user')->get());
    }
}
