<?php

namespace App\Http\Controllers;

use App\Material;
use App\Category;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    public function index()
    {
        return view('materials.index');
    }

    public function create()
    {
        
    }

    public function store(Request $request)
    {
        Material::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Materiaal opgeslagen'
        ]);
    }

    public function show(Material $material)
    {
        return view('materials.view', compact('material'));
    }

    public function edit(Material $material)
    {
        //
    }

    public function update(Request $request, Material $material)
    {
        $material->fill($request->input())->save();

        return json_encode([
            'status' => 1,
            'message' => 'Materiaal bijgewerkt.'
        ]);
    }
    
    public function destroy(Material $material)
    {
        $material->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Materiaal verwijderd.'
        ]);
    }

    public function getMaterials() 
    {
        $categories = Category::with('materials')->get()->push([
            'materials' => Material::where('category_id', 0)->get(),
            'name' => 'Overige',
            'id' => 0
        ]);
        
        return json_encode($categories);
    } 
}
