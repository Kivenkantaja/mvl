<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMwToHours extends Migration
{
    public function up()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->integer('mw')->default(0);
        });
    }

    public function down()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->dropColumn('mw');
        });
    }
}
