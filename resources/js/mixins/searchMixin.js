export const searchMixin = {
    methods: {
        search(collection, input, fields) {
            if(!input) {
                return collection;
            }

            return _.filter(collection, item => {
                return this.find(fields, item, input);
            });
        },

        find(fields, item, input) {
            let found = false;

            fields.map(field => {
                // if(item[field] && item[field].toLowerCase().includes(input.toLowerCase())) {
                // 	found = true;
                // }

                if(this.check(item[field], input)) {
                    found = true;
                }
            });

            return found;
        },

        check(item, input) {
            if(item) {
                let parts = input.split(' ');
                let found = true;

                parts.map(part => {
                    if(!item.toLowerCase().includes(part.toLowerCase())) {
                        found = false;
                    }
                });

                return found;
            }
        }
    }
}
