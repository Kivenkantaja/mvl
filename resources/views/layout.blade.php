<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Martin van Leeuwen</title>
	<link rel="stylesheet" href="/css/app.css">
	<link rel="manifest" href="/manifest.json">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="#">
</head>
<body>
	<div id="app" v-cloak>
		@if(Auth::user())
			<div class="navbar">
				<div class="col-md-12 np">
					<div class="pull-right">
						<button v-on:click="hourForm = {}" class="btn btn-navbar">
							<i class="fas fa-plus"></i>
						</button>

                        <button v-on:click="showTimers = true" class="btn btn-navbar">
                            <i class="fas fa-clock"></i>
                        </button>

						<button v-on:click="showMenu = !showMenu" id="menu" class="btn btn-navbar">
							<i class="fas fa-bars"></i>
						</button>
					</div>
				</div>
			</div>

			<div :class="{'show-menu': showMenu}" class="sidebar">
				<nav-link link="/" icon="fas fa-home">Home</nav-link>

				@if(Auth::user()->is_admin)
					<nav-link link="/clients" icon="fas fa-users">Klanten</nav-link>
					<nav-link link="/projects" icon="fas fa-project-diagram">Projecten</nav-link>
					<nav-link link="/categories" icon="fas fa-list">Categorieën</nav-link>
					<nav-link link="/materials" icon="fas fa-tools">Materialen</nav-link>
				@endif

				<nav-link link="/hours" icon="fas fa-clock">Uren </nav-link>
                <nav-link link="/borrowed-items" icon="fas fa-toolbox">Uitgeleend</nav-link>

				@if(Auth::user()->is_admin)
					<nav-link link="/feedback" icon="fas fa-comments">Feedback</nav-link>
					<nav-link link="/users" icon="fas fa-users">Gebruikers</nav-link>
				@endif
			</div>

		@endif

		<div class="{{ Auth::user() ? 'content' : 'login' }}">
			<div v-if="message" class="row">
				<div class="col-md-12">
					<div class="alert" :class="{'alert-success': message.status == 1, 'alert-danger': message.status == 0}">
						@{{ message.message }}

						<div class="pull-right">
							<button v-on:click="message = null" class="btn-table">
								<i class="fas fa-times"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
			@yield('content')
		</div>

		@if(Auth::user())
			<timers user_id="{{ Auth::user()->id }}"
					v-on:form="hourForm = $event"
					v-on:close="showTimers = false"
					v-if="showTimers">
			</timers>

			<hour-form v-on:saved="handleSaved($event)"
					   v-on:error="message = $event; hourForm = null"
					   v-on:close="hourForm = null"
					   v-if="hourForm"
					   :data="hourForm"
					   current_user="{{ Auth::user() }}">
			</hour-form>
		@endif

	</div>

	<script src="/js/app.js"></script>
</body>
</html>
