<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function index()
    {
        return view('feedback.index');
    }

    public function getFeedback(Request $request) 
    {
        return Feedback::where('status', $request->status)->get();
    }

    public function store(Request $request)
    {
        Feedback::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Feedback opgeslagen.'
        ]);
    }

    public function update(Request $request, Feedback $feedback)
    {
        $feedback->update($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Feedback bijgewerkt.'
        ]);
    }

    public function destroy(Feedback $feedback)
    {
        $feedback->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Feedback verwijderd.'
        ]);
    }
}
