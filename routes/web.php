<?php

Auth::routes(['register' => false]);
Route::get('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'HomeController@index');
	Route::get('dayReports', 'HomeController@dayreports');

	Route::resource('clients', 'ClientController');
	Route::get('getClient/{client}', 'ClientController@getClient');
	Route::get('getClients', 'ClientController@getClients');

	Route::resource('projects', 'ProjectController');
	Route::get('getProject/{project}', 'ProjectController@getProject');
	Route::get('getProjects', 'ProjectController@getProjects');

	Route::resource('hours', 'HourController');
	Route::get('getHours', 'HourController@getHours');
	Route::get('getHoursOverview', 'HourController@getHoursOverview');
	Route::get('hoursByProject/{project_id}', 'HourController@hoursByProject');

	Route::resource('timers', 'TimerController');

	Route::resource('notes', 'NoteController');

	Route::resource('materials', 'MaterialController');
	Route::get('getMaterials', 'MaterialController@getMaterials');

	Route::resource('categories', 'CategoryController');
	Route::get('getCategories', 'CategoryController@getCategories');

	Route::resource('feedback', 'FeedbackController');
	Route::get('getFeedback', 'FeedbackController@getFeedback');

	Route::resource('users', 'UserController');
	Route::get('getUsers', 'UserController@getUsers');

	Route::resource('reminders', 'ReminderController');

	Route::resource('borrowed-items', 'BorrowedItemController');
	Route::get('getBorrowedItems', 'BorrowedItemController@getBorrowedItems');
});
