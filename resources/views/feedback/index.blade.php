@extends('layout')

@section('content')
	<h1>Feedback</h1>
	<feedback v-on:message="message = $event" 
			  status="open" 
			  v-on:refresh="$refs['feedbackDone'].loadData()">		  	
	</feedback>
	<feedback v-on:message="message = $event" 
			  status="done" 
			  ref="feedbackDone">		  	
	</feedback>
@stop