import Moment from 'moment';
import 'moment/locale/nl';
import {extendMoment} from 'moment-range';
const moment = extendMoment(Moment);
moment.locale('nl')

export const momentMixin = {
	methods: {
		range(first, last) {
			return Array.from(moment.range(first, last).by('day'));
		},

		weekRange(filters) {
			let last = (filters && filters.toDate) ? moment(filters.toDate) : moment().add(1, 'days');
			let first = (filters && filters.fromDate) ? moment(filters.fromDate) : moment(last).subtract(1, 'year');

			let range = Array.from(moment.range(first, last).by('week'));

			// First make a range of weeks, then fill that array with an object with the daterange of said weeks
			let array = _.map(range, date => {
			    date.day(1);

			    let week = moment(date).format('W');
			    let year = moment(date).format('YYYY');

				return {
					week: parseInt(week),
					year: parseInt(year),
					days: this.rangeByWeek(date).map(day => {
					   	   return {
					   	   		date: day,
					   	   		hours: []
					   	   }
					   }),
				}
			}).reverse();

			return array;
		},

		rangeByWeek(date) {
			let first = moment(date).day(1);
			let last = moment(date).day(7);

			return this.range(first, last);
		},

		fillWeeksArray(weeks, hours) {
			return weeks.map(week => {
				return {
					week: week.week,
					year: week.year,
					days: week.days.map(day => {
						let date = day.date.format('YYYY-MM-DD');

						return {
							date: day.date,
							hours: _.sortBy(_.filter(hours, {date: date}), ['startTime'])
						}
					})
				}
			});
		}
	}
}
