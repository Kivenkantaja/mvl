export const globalMixin = {
	data() {
		return {
			message: null
		}
	},
	methods: {
		array_sum(arr) {
			return arr.reduce((a,b) => a + b, 0)
		},

		leadingZero(number) {
		    if(number < 10)
		    {
		        return '0' + number.toString();
		    } else
		    {
		        return number;
		    }
		},

		save(url, data) {
			if(data.id) {
				axios.put(url+'/'+data.id, data).then(response => {
					this.$emit('saved', response.data);
				}).catch(error => {
					this.$emit('error', {
						status: 0,
						message: 'Er is een fout opgetreden.'
					});
				});
			} else {
				axios.post(url, data).then(response => {
					this.$emit('saved', response.data);
				}).catch(error => {
					this.$emit('error', {
						status: 0,
						message: 'Er is een fout opgetreden.'
					});
				});
			}
		},

		destroy(url, id) {
			axios.delete(url+'/'+id).then(response => {
				this.confirmDelete = null;
				this.loadData();
				this.$emit('message', response.data);
			});
		},

		reverseDate(date) {
			return date.split('-').reverse().join('-');
		},

		hoursToMinutes(time) {
			let input = time.split(':');
			return parseInt(input[0] * 60) + parseInt(input[1]);
		},

		formatHours(minutes) {
			let min = this.leadingZero(minutes % 60);
			let hours = parseInt(minutes / 60);

			return hours + ':' + min;
		},

		handleError(event) {
			if(this.form) {
				this.form = null;
			}
			this.$emit('message', event);
		},

		handleDeleted(array, relation) {
        	array.map((e, key) => {
        		if(!e[relation]) {
        			array[key][relation] = {name: 'Verwijderd'}
        		}
        	});

        	return array;
        },

		displayHours(hour) {
			let start = this.hoursToMinutes(hour.startTime);
			let end = this.hoursToMinutes(hour.endTime);

			let min = end - start;

            const breakMinutes = isNaN(hour.break_minutes) ? 0 : hour.break_minutes;

			return this.formatHours(min - breakMinutes || 0);
		},

		description(desc) {
			return desc ? desc : 'Geen omschrijving ingevoerd';
		},

		timeToMin(time) {
			let hours = parseInt(time.split(':')[0]) * 60;
			let min = parseInt(time.split(':')[1]);
			return hours + min;
		},

		contains(array, index) {
			let filtered = _.filter(array, e => { return e[index] });
			return filtered.length > 0;
		},

		findName(collection, id) {
			let item = _.filter(collection, {id: id});

			if(item[0]) {
				return item[0].name || null;
			}
		},

		checkClient(hour) {
			if(hour.client) {
				return hour.client ? hour.client.name : 'Verwijderde klant';
			}

			if(hour.project) {
				return hour.project.client ? hour.project.client.name : 'Verwijderde klant of project'
			}

			return 'Verwijderde klant'
		},

		getHourType(hour) {
			if(hour.recording) {
				return '(wo)';
			} else if(hour.break) {
				return '(P)';
			} else if(hour.mw) {
				return '(mw)';
			} else {
				return null
			}
		},

		copy(x) {
			return JSON.parse(JSON.stringify(x));
		},

		displayDate(date) {
			return moment(date).format('DD-MM-YYYY');
		}
	},

	watch: {
		message(val) {
			if(val) {
				window.setTimeout(() => {this.message = null}, 2000)
			}
		}
	},

	components: {
		notification: require('../components/ui/notification.vue').default
	}
}
