<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHoursTable extends Migration
{
    public function up()
    {
        Schema::create('hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->longText('description')->nullable();
            $table->string('date');
            $table->string('startTime');
            $table->string('endTime');
            $table->integer('project_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('hours');
    }
}
