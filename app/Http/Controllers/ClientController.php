<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
         return view('clients.index');
    }

    public function getClients() 
    {
        $clients = Client::with('projects')->get();

        return json_encode($clients);
    }

    public function getClient(Client $client) 
    {
        return json_encode($client);
    }

    public function show(Client $client) 
    {
        return view('clients.view', compact('client'));        
    }

    public function store(Request $request)
    {
        $client = new Client;
        $client->fill($request->input());
        $client->save();

        return json_encode((object) [
            'status' => 1,
            'message' => 'Nieuwe klant aangemaakt.'
        ]);    
    }

    public function update(Request $request, Client $client)
    {
        $client->fill($request->input())->save();

        return json_encode((object) [
            'message' => 'Klant aangepast.',
            'status' => 1
        ]);
    }

    public function destroy(Client $client)
    {
        $client->delete();

        return json_encode((object) [
            'message' => 'Klant verwijderd', 
            'status' => 1
        ]);
    }
}
