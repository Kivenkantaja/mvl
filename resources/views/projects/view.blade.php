@extends('layout')

@section('content')
	<project v-on:form="hourForm = $event"
			 v-on:message="message = $event" 
			 project_id="{{ $project->id }}"
			 ref="project">		 	
	</project>
@stop