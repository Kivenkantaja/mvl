<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function index()
    {
        return json_encode(Note::get());
    }

    public function store(Request $request)
    {
        Note::create($request->input());

        return json_encode([
            'status' => 1,
            'message' => 'Notitie opgeslagen.'
        ]);
    }

    public function show(Note $note)
    {
        //
    }

    public function update(Request $request, Note $note)
    {
        $note->fill($request->input())->save();

        return json_encode([
            'status' => 1,
            'message' => 'Notitie bijgewerkt.'
        ]);
    }

    public function destroy(Note $note)
    {
        $note->delete();

        return json_encode([
            'status' => 1,
            'message' => 'Notitie verwijderd.'
        ]);
    }
}
