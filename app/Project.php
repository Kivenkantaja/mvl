<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
	use SoftDeletes;

    protected $fillable = ['client_id', 'name', 'status'];

    public function client()
    {
    	return $this->belongsTo('App\Client');
    }

    public function hours()
    {
    	return $this->hasMany('App\Hour');
    }
}
