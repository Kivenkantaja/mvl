@extends('layout')

@section('content')
    <borrowed-items v-on:message="message = $event" :user="{{ Auth::user() }}"></borrowed-items>
@stop
