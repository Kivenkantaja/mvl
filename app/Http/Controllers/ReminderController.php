<?php

namespace App\Http\Controllers;

use App\Reminder;
use Illuminate\Http\Request;

class ReminderController extends Controller
{
    public function index()
    {
        return json_encode(Reminder::get()); 
    }

    public function store(Request $request)
    {
        return json_encode([
            'reminder' => Reminder::create($request->input()),
            'message' => 'Herhinnering opgeslagen.'
        ]);
    }

    public function show(Reminder $reminder)
    {
        return json_encode($reminder);
    }

    public function update(Request $request, Reminder $reminder)
    {
        return json_encode([
            'reminder' => $reminder->update($request->input()),
            'message' => 'Herhinnering bijgewerkt.'
        ]);
    }

    public function destroy(Reminder $reminder)
    {
        return json_encode([
            'status' => 1,
            'reminder' => $reminder->delete(),
            'message' => 'Herhinnering verwijderd.'
        ]);
    }
}
