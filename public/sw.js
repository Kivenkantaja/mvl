self.addEventListener('fetch', function(event) {
    caches.keys().then(function(names) {
        for (let name of names)
            caches.delete(name);
    });
})

self.addEventListener('activate', function(event) {

})