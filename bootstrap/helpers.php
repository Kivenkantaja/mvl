<?php

function formatHours($min) 
{
    $hours = (int)($min / 60);
    $minutes = sprintf('%02d', $min % 60);    

    return "$hours:$minutes";
}

function dateRange($start, $end)
{
    $begin = new DateTime($start);
    $end = new DateTime($end);
    $end = $end->modify( '+1 day' );

    $interval = new DateInterval('P1D');
    $daterange = new DatePeriod($begin, $interval ,$end);

    $output = array();
    foreach($daterange as $date)
    {
        $output[] = $date->format('Y-m-d');
    }

    return $output;    
}
