<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->longText('text');
            $table->integer('type'); // 0 = opmerking 1 = bug
            $table->integer('status')->default(0);
            $table->string('hours')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
