import Datepicker from 'vuejs-datepicker';
import {nl} from 'vuejs-datepicker/dist/locale';

export const datepickerMixin = {
	methods: {
        dateFormat(date) {
          return moment(date).format('DD-MM-YYYY');
        },

		formatDate(date, reverseDate = false) {
		    let array = [
		        this.leadingZero(date.getDate()),
		        this.leadingZero(date.getMonth() + 1),
		        date.getFullYear(),
		    ];

		    let output = array.join('-');

		    if(reverseDate) {
		    	output.split('-').reverse().join('-');
		    }

		    return output;
		},
	},

	components: {
		Datepicker
	},

	data() {
		return {
			nl: nl
		}
	}
}
