<?php

namespace App\Http\Controllers;

use App\BorrowedItem;
use Illuminate\Http\Request;

class BorrowedItemController extends Controller
{
    public function index()
    {
        return view('borrowed_items.index');
    }

    public function getBorrowedItems()
    {
        return response()->json(BorrowedItem::get());
    }

    public function store(Request $request)
    {
        $borrowedItem = BorrowedItem::create($request->input());

        return response()->json([
            'status' => 1,
            'data' => $borrowedItem,
            'message' => 'Uitgeleende spullen opgeslagen',
        ]);
    }

    public function show(BorrowedItem $borrowedItem)
    {
        return response()->json($borrowedItem);
    }

    public function update(Request $request, BorrowedItem $borrowedItem)
    {
        $borrowedItem->update($request->input());

        return response()->json([
            'status' => 1,
            'data' => $borrowedItem,
            'message' => 'Uitgeleende spullen bijgewerkt',
        ]);
    }

    public function destroy(BorrowedItem $borrowedItem)
    {
        $borrowedItem->delete();

        return response()->json([
            'status' => 1,
            'message' => 'Uitgeleende spullen verwijderd',
        ]);
    }
}
