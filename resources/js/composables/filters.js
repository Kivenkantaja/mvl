import {ref, watch} from "@vue/composition-api";

export default function () {
    let find = (fields, item, input) => {
        return !!_.find(fields, field => checkString(item[field], input));
    };

    let checkString = (item, input) => {
        return !_.find(input.split(' '), part => {
            return !item.toString().toLowerCase().includes(part.toLowerCase());
        });
    };

    let search = ref(null);
    let applySearch = (collection, string, fields) => {
        return !string ? collection : _.filter(collection, archive => {
            return find(fields, archive, string);
        });
    };

    let getOptions = (collection, column) => {
        return _(collection).map(item => item = item[column]).compact().uniqBy().value();
    };

    let fieldsToFilter = ref({});
    let applyFilters = (collection) => {
        _.forEach(fieldsToFilter.value, (filter, key) => {
            collection = filter.length === 0 ? collection : _.filter(collection, item => {
                return _.includes(filter, item[key])
            });
        });

        return collection;
    };

    return {search, fieldsToFilter, getOptions, applyFilters, applySearch};
}
