import _ from "lodash";

require('./bootstrap');

import lodash from 'lodash';
import moment from 'moment';
import axios from 'axios';
import Vue from "vue";

window._ = lodash;
window.Vue = Vue;
window.axios = axios;
window.moment = moment;

import { globalMixin } from './mixins/globalMixin';

let test = 'test';

Vue.component('modal', require('./components/ui/modal.vue').default);
Vue.component('form-item', require('./components/ui/form-item.vue').default);
Vue.component('form-checkbox', require('./components/ui/form-checkbox.vue').default);
Vue.component('confirm', require('./components/ui/confirm.vue').default);
Vue.mixin(globalMixin);

if('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
             .then(() => console.log('Service worker works'))
             .catch(() => console.log('Service worker denied'));
}

const app = new Vue({
    el: '#app',

    data: {
    	message: null,
        showMenu: false,
        showTimers: false,
        hourForm: null,
    },

    watch: {
    	message(val) {
    		if(val && val.status == 1) {
    			window.setTimeout(() => {
    				this.message = null
    			}, 2000)
    		}
    	}
    },

    methods: {
        refresh(list) {
            if(this.$refs[list]) {
                this.$refs[list].handleSaved();
            }
        },

        handleSaved($event) {
            this.hourForm = null;
            this.message = $event;

            let refresh = ['hours', 'client', 'project'];
            refresh.map(e =>{
                this.refresh(e);
            });
        },

        reload() {
            window.location.reload(true);
        }
    },

    components: {
    	navLink: require('./components/ui/nav-link.vue').default,
        client: require('./components/clients/view.vue').default,
    	clients: require('./components/clients/index.vue').default,
        project: require('./components/projects/view.vue').default,
        projects: require('./components/projects/list.vue').default,
        hours: require('./components/hours/list.vue').default,
        timers: require('./components/hours/timers.vue').default,
        hourForm: require('./components/hours/form.vue').default,
        material: require('./components/materials/view.vue').default,
        materials: require('./components/materials/list.vue').default,
        category: require('./components/categories/view.vue').default,
        categories: require('./components/categories/list.vue').default,
        overviews: require('./components/home/overviews.vue').default,
        feedback: require('./components/feedback/list.vue').default,
        user: require('./components/users/view.vue').default,
        users: require('./components/users/list.vue').default,
        reminders: require('./components/reminders/overview.vue').default,
        borrowedItems: require('./components/borrowed-items/index.vue').default,
    }
});
