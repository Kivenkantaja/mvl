<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToHours extends Migration
{
    public function up()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->integer('user_id')->default(2);
        });
    }

    public function down()
    {
        Schema::table('hours', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
